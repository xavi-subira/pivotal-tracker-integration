<?php
include 'Pivotal_API_V5.php';


class BitbucketListener{

    public function listen($POST_DATA){
        $data = $POST_DATA ? : $_POST;
        // Interpretar data
        try{
            $push_object = json_decode($data);
        }catch(\Exception $e)
        {
            die($e->getMessage());
        }

        // Obtener el array de commits de dentro de $push_object
        $commitsArray = $push_object->push->changes[0]->commits;
        // Recorrer el array, crear instancias de tipo CommitObject y llamar a sendCurl
        foreach($commitsArray as $bitbucketcommit):
            //crear el commitObject según ITBID
            $id = $bitbucketcommit->hash;
            $msg = $bitbucketcommit->message;
            $author = $bitbucketcommit->author;
            $commitObject = new CommitObject($id, $msg, $author);

            sendCurl(PIVOTAL_TOKEN,$commitObject,'https://www.pivotaltracker.com/services/v5/source_commits?fields=%3Adefault%2Ccomments');
        endforeach;
    }
}

return new BitbucketListener();