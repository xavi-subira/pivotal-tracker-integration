<?php

class CommitObject{
    /**
     * Identificador tipo hash sha-1 del commit
     */ 
    public $commit_id;
    /**
     * Cuerpo del commit escrito por el author
     * que puede (debe) contener un identificador de Story de pivotal
     * */
    public $commit_msg; 

    /**
     * Url para acceder al commit en bitbucket
     */   
    public $commit_url;

    /**
     * Author del commit
     */
    public $commit_author;

    /**
     * Fecha (momento) del commit
     */ 
    public $commit_date;

    public function __construct($id,$msg,$author,$date = null){
      $this->commit_id = (string)$id;
      $this->commit_msg= $msg;
      $this->commit_author=$author;
      $this->commit_date=$date;

      $this->commit_url = "https://bitbucket.org/itbid/itbid-prod/commits/{$id}";

    }

}

