<?php
include('Classes/CommitObject.php');
const PIVOTAL_TOKEN = 'ebe3af33bfc5ef5dac036ae940c4b4cd';
$pivotalUrl = 'https://www.pivotaltracker.com/services/v5/source_commits?fields=:default,comments';
function sendToPivotalTracker($TOKEN, CommitObject $commit)
{
    // Abrimos el $commit i construimos el parámetro "d" de la llamada a curl que queremos hacer


    // Ejecutamos curl con parámetor igual que este ejemplo para envío de datos a la API V5 de Pivotal
    $comando =
<<<HEREDOC
        curl -X POST -H "X-TrackerToken: $TOKEN" -H "Content-Type: application/json" -d '{"source_commit":{"commit_id":"{$commit->commit_id}","message":"{$commit->commit_msg}","url":"{$commit->commit_url}","author":"{$commit->commit_author}"}}' "https://www.pivotaltracker.com/services/v5/source_commits?fields=%3Adefault%2Ccomments"
HEREDOC;


    exec($comando, $output, $returnval);

}

function sendCurl($TOKEN, CommitObject $commit, $pivotalUrl){

    $data = (array)$commit;
    $data = array_combine(["commit_id","message","url","author", "date"],$data);
    unset($data["date"]);
    $data = Array("source_commit" => $data);

    $postData = json_encode($data);
    print_r($postData);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , 'X-TrackerToken: '.$TOKEN ));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    //curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch,CURLOPT_URL,$pivotalUrl);
    $output=curl_exec($ch);
    $header = curl_getinfo($ch, CURLINFO_HEADER_OUT);
    print_r($header);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($output, 0, $header_size);
    print_r($header);
    $body = substr($output, $header_size);
    print_r($body);
    curl_close($ch);

    return $output;
}



